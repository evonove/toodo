__author__ = 'syn'

from collections import OrderedDict

from toodo import db


class Toodo(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(1000))
    date = db.Column(db.DateTime, nullable=False)
    active = db.Column(db.Boolean, default=True)

    def serialize(self):
        result = OrderedDict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)

        result['date'] = self.date.isoformat()

        return result

