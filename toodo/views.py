__author__ = 'syn'

from datetime import datetime

from flask import render_template, jsonify, request, abort
from flask.views import MethodView

from toodo import app, db
from toodo.models import Toodo


@app.route('/')
def index():
    return render_template('index.html')


class ToodoAPI(MethodView):
    def get(self, toodo_id):
        if toodo_id:
            toodo = Toodo.query.get_or_404(toodo_id)
            return jsonify(toodo.serialize())
        else:
            # get params from query string
            active = request.args.get("active")
            fromdate = request.args.get("fromdate")

            # build query
            q = Toodo.query
            if active:
                a = True if active == 'true' else False
                q.filter(Toodo.active == a)

            if fromdate:
                d = datetime.strptime(fromdate, "%Y-%m-%d")
                q.filter(Toodo.date > d)

            result = [toodo.serialize() for toodo in q.all()]
            return jsonify({'objects': result})

    def post(self):
        title = request.values.get("title")
        if not title:
            abort(400)

        description = request.values.get("description")

        toodo = Toodo(title=title, description=description, date=datetime.now())

        db.session.add(toodo)
        db.session.commit()

        return jsonify(toodo.serialize())

    def delete(self, toodo_id):
        toodo = Toodo.query.get_or_404(toodo_id)

        db.session.delete(toodo)
        db.session.commit()
        return ""


# toodo api setup
toodo_view = ToodoAPI.as_view('toodos_api')

app.add_url_rule('/toodos/', defaults={'toodo_id': None},
    view_func=toodo_view, methods=['GET',])
app.add_url_rule('/toodo/<int:toodo_id>', view_func=toodo_view,
    methods=['GET', 'DELETE'])
app.add_url_rule('/toodo/', view_func=toodo_view, methods=['POST',])
