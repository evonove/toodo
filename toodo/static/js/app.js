Toodos = Em.Application.create({
    // When everything is loaded.
    ready: function() {

        Toodos.searchResults.refresh();

        // Start polling Toodo
        setInterval(function() {
            Toodos.searchResults.refresh();
        }, 2000);

        // Call the superclass's `ready` method.
        this._super();
    }
});

Toodos.Toodo = Em.Object.extend({
    id: null,
    title: null,
    description: null,
    active: null,
    date: null,

    save : function() {
        $.ajax({
            url: '/toodo',
            type: 'post',
            contentType:"application/json",
            data: JSON.stringify(content)
        })
            .done(function(data) {
                console.log("Entity "+ data.id +" saved");

                if (typeof(callback) == 'function')
                    callback(self);
            })
            .fail(function(data) {
                console.log(data);
            });
    }
});

Toodos.searchResults = Em.ArrayController.create({
    content: [],
    query: null,
    _idCache: {},

    // Add a Toodos.Toodo instance to this collection.
    // Most of the work is in the built-in `pushObject` method,
    // but this is where we add our simple duplicate checking.
    addTweet: function(toodo) {
        // The `id` from Toodo's JSON
        var id = toodo.get("id");

        // If we don't already have an object with this id, add it.
        if (typeof this._idCache[id] === "undefined") {
            this.insertAt(0, toodo);
            this._idCache[id] = toodo.id;
        }
    },

    refresh: function() {
        var query = this.get("query");

        // Poll Toodo
        var self = this;
        var url = "/toodos?" + query;
        $.getJSON(url, function(data) {

            // Make a model for each result and add it to the collection.
            $(data.objects).each(function(index, value){
                var d = new Date(value.date);

                self.addTweet(Toodos.Toodo.create({
                    id : value.id,
                    title: value.title,
                    description: value.description,
                    active: value.active,
                    date: d.toGMTString()
                }));
            })
        });
    }.observes("query")
});
