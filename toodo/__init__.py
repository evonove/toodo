__author__ = 'syn'

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

# application setup
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///toodo.db'
app.config['DEBUG'] = True

# sqlalchemy setup
db = SQLAlchemy(app)
import toodo.models

# initialize database
db.create_all()

import toodo.views